#!/bin/bash

set -u

# This script wraps 'git push' and writes git debug information to a log file.
# It also calculates some basic statistics about the repository.
#
# This includes:
#   * The number of files in the repository
#   * The estimated repository size
#   * Number of commits
#   * Number of refs
#   * Number of objects
#   * Number of contributors

LOG_FILE="atlassian-stash-git-debug.log"

export GIT_TRACE=1
export GIT_TRACE_PACKET=1
export GIT_CURL_VERBOSE=1

echo "Debuging git push - `date`" >> $LOG_FILE
git push --verbose "$@" 2>&1 | grep -v "^Authorization:" | tee -a $LOG_FILE

# ==================================================================================
# Capture some statistics (based on https://github.com/juretta/git-pastiche/blob/master/bin/git-stats)

# Summarize information about the current git repository
NUMBER_OF_COMMITS=`git rev-list --all | wc -l`
NUMBER_OF_OBJECTS=`git count-objects -v | grep in-pack | cut -f 2 -d ':'`
NUMBER_OF_FILES=`git ls-files | wc -l | tr -d ' '`
NUMBER_OF_CONTRIBUTORS=`git shortlog -s -n | wc -l | tr -d ' '`
NUMBER_OF_REFS=`git show-ref --heads -s | wc -l | tr -d ' '`
AGE=`git log --reverse --format='%cr' | head -n 1`
REPO_SIZE=`du -hs $(git rev-parse --git-dir) | awk '{print $1}'` # count-objects has size-pack but that _requires_ the repository to be packed

stats=$(
printf "Repository size (approx): %12s\n"   "${REPO_SIZE}"
printf "Number of contributors  : %'12.d\n" $NUMBER_OF_CONTRIBUTORS
printf "Number of commits       : %'12.d\n" $NUMBER_OF_COMMITS
printf "Number of objects       : %'12.d\n" $NUMBER_OF_OBJECTS
printf "Number of files         : %'12.d\n" $NUMBER_OF_FILES
printf "Number of refs          : %'12.d\n" $NUMBER_OF_REFS
printf "First commit            : %12s\n"   "${AGE}"
)

echo "$stats" | tee -a $LOG_FILE
