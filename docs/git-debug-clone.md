% git-debug-clone(1) User manual
% Atlassian
% February, 2013

# NAME

git debug-clone - Debug wrapper for the git clone command

# SYNOPSIS

git debug-clone [...] `repository`

# DESCRIPTION

git debug-clone is a wrapper script for the clone command that can be used to
log valuable debug information to a local log file named
*atlassian-stash-git-debug.log*.

It captures client side debug information by setting the `GIT_TRACE`,
`GIT_TRACE_PACKET` and `GIT_CURL_VERBOSE` environment variables. Debug output
will be writte to stdout and a pre-defined log file.

To ensure that the log file can safely be attached to a support ticket or
copied into public forums, the Authorization header of the HTTP request (if the
smart HTTP transport is being used) will be stripped and _not_ written to the
log file.

The command accepts the same options as `git clone` and simply passes them
through. The basic usage boils down to using `git debug-clone` instead of `git
clone`.

    git debug-clone ssh://git@bitbucket.org/atlassian/git-client-debug.git

# SEE ALSO

The source code may be downloaded from 
<https://bitbucket.org/atlassian/git-client-debug>.

